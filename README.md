# Validating Flyway migrations with GitLab

Repository supporting [a blog post](https://findingpaths.io/blog/validating-flyway-schema-definitions-with-gitlab-cicd/) on validating Flyway migrations in a GitLab CI/CD pipeline.
